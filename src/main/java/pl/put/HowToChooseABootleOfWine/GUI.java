package pl.put.HowToChooseABootleOfWine;

import javax.swing.JOptionPane;

import pl.put.HowToChooseABootleOfWine.Question;

public class GUI {
	public void runQuestionPrompt(Question question) {
		int x = JOptionPane.showOptionDialog(
				null, 
				question.getQuestion(),
                "Click a button",
                JOptionPane.DEFAULT_OPTION, 
                JOptionPane.INFORMATION_MESSAGE, 
                null, 
                question.getOptions().toArray(), 
                null
			);
		question.setAnswer(question.getOptions().get(x));
	}
	
	public void runResultPrompt(Question result) {
		System.out.println(result.getQuestion());
		JOptionPane.showMessageDialog(null,
				result.getAnswer(),
			    "Result",
			    JOptionPane.PLAIN_MESSAGE);
		result.setQuestion("TERMINATE");
	}
}
