package pl.put.HowToChooseABootleOfWine;

import java.util.ArrayList;

public class Question {
	private String answer = "";
	private String question = "";
	private ArrayList<String> options = new ArrayList<>();
	
	public String getAnswer() {
		return answer;
	}
	
	public String getQuestion() {
		return question;
	}
	
	public ArrayList<String> getOptions() {
		return options;
	}
	
	public void setAnswer(String anwser) {
		this.answer = anwser;
	}
	
	public void setQuestion(String question) {
		this.question = question;
	}
	
	public void setOptions(ArrayList<String> options) {
		this.options = options;
	}
	
	public void addYesNo() {
		ArrayList<String> options = new ArrayList<String>();
		options.add("Yes");
		options.add("No");
		setOptions(options);
	}
}
